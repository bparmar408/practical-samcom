import './App.css';
import { useState } from 'react';


function AddUser() {

  const [user,setUser] =useState({first_name:'',last_name:'',email:'',avtar:''})
  const [userErrors,setUserErrors] = useState({})

  const validate = ({value='',type='',isSubmit=false})=>{
    // in case of submit need to check validatin for all fields
    if(isSubmit)
    {
      // in case user need count on validating fields
      let errorCount = 0
      const arrFields =['first_name','last_name','email']
      arrFields.forEach(element => {
        setUserErrors({...userErrors,[type]: user[element] ? '' : 'required'})
        if(user[element] == '')
        {
          errorCount = errorCount + 1
        }
      });
     return errorCount > 0? false :true
    }else{
      // single field validation on blur
      setUserErrors({...userErrors,[type]: value ? '' : 'required'})
    }
  }

  const submitForm = ()=>{
      fetch('https://reqres.in/api/users',{
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(user) 
      }).then((res)=>{
        alert('data posted successfully')
      }).catch((err)=>{
        alert('error occured ')
      })
  }
  return (
    <>
        <div className='top-bar'>
          Add user        </div>
        <div className='list-container'>
          <div className='item'>
            <span className='name'> First Name</span>
            <input onChange={(e)=>{
              setUser({...user,['first_name']: e.target.value})
            }} value={user.first_name} className={`text ${userErrors['first_name'] &&  'validate'}`} onBlur={(e)=>{
              validate({value:e.target.value,type:'first_name'})
            }} type="text"/>
          </div>
          <div className='item'>
            <span className='name'> Last Name</span>
            <input input onChange={(e)=>{
              setUser({...user,['last_name']: e.target.value})
            }}
            onBlur={(e)=>{
              validate({value:e.target.value,type:'last_name'})
            }} value={user.last_name} className={`text ${userErrors['last_name'] && 'validate'}`} type="text"/>
          </div>
          <div className='item'>
            <span className='name'> Email</span>
            <input input onChange={(e)=>{
              setUser({...user,['email']: e.target.value})
            }}
            onBlur={(e)=>{
              validate({value:e.target.value,type:'email'})
            }} value={user.email} className={`text ${userErrors['email'] && 'validate'}`} 
            type="text"/>
          </div>
          <div className='item'>
            <button onClick={()=>{
              if(validate({isSubmit:true}))
              {
                submitForm()
              }else{
                alert('please validate fields')
              }
            }}>Submit</button>
          </div>
        </div>
    </>
  );
}

export default AddUser;
