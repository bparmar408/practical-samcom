import {  useContext, useEffect, useState } from 'react';
import AppContext from './appCotext';
import delIcon from './images/delete.png'


function HomePage() {
  
 const context = useContext(AppContext)
 // defines the current selected item, it helps to delete specific item from list
 const [activeItem,setActiveItem] = useState(0)
  useEffect(()=>{
      fetch('https://reqres.in/api/users')
      .then((res)=> {return res.json()})
      .then((data)=>{
        context.setUsers(data.data.sort())
        sortArray(data.data)
      })
  },[])

  const handleDelete = (id)=>{
      const userItems = [...context.users]
      userItems.splice(activeItem,1)
      context.setUsers(userItems)

      // in case selected item is last one, need to set last item of array
      if(activeItem >= userItems.length && userItems.length > 0)
      {
        setActiveItem(activeItem-1)
      }
  }

  const onSort = (sortType)=>{
    sortArray(context.users,sortType)
}

const sortArray = (data,sortType=1)=>{
  context.setUsers([...data
    .sort((a, b) => {
      return sortType == 1 ? (a.first_name > b.first_name ? 1 : a.first_name < b.first_name ? -1 : 0):(a.first_name > b.first_name ? -1 : a.name < b.name ? 1 : 0)
   })])
}

  return (
      <>
        <div className='top-bar'>
          <select onChange={(e)=>{onSort(parseInt(e.target.value))}}>
              <option value="1">sort A to Z</option>
              <option value="2">sort Z to A</option>
          </select>

          <img onClick={handleDelete} src={delIcon} />
        </div>
        <div className='list-container'>
          {
            context.users.map((item,index)=>{
              return(
                <div onClick={()=>{
                  setActiveItem(index)
                }} key={index} className={`item ${activeItem == index && 'active'}`}>
            <img src={item.avatar}/>
            <span className='name'> {`${item.first_name} ${item.last_name}`}</span>
          </div>
              )
            })
          }
        </div>
      </>
  );
}

export default HomePage;
