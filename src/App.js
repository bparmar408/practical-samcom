import './App.css';
import { useState } from 'react';
import HomePage from './homePage';
import appContext from './appCotext';
import AddUser from './addUser';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import NavigationBar from './navigationBar;';

function App() {
  
const [users,setUsers] = useState([])
  return (
    <appContext.Provider value={{users:users,setUsers : (data)=>{
      setUsers(data)
    }}}>
      <div className='main'>
      <BrowserRouter>
      <Routes>
        <Route path="/" element={<NavigationBar />}>
          <Route path="/home" element={<HomePage />} />
          <Route path="/adduser" element={<AddUser />} />
        </Route>
      </Routes>
    </BrowserRouter>
      </div>
    </appContext.Provider>
  );
}

export default App;
