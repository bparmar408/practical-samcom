import './App.css';
import { useState } from 'react';
import { Outlet, Link } from "react-router-dom";


function NavigationBar() {
  return (
    <>
    <ul className='navbar'>
      <li>
      <Link to="/home">Home</Link>{"   "}
      </li>
      <li>
      <Link to="/adduser">Add User</Link>
      </li>
    </ul>


<Outlet/>
    </>
      );
}

export default NavigationBar;
